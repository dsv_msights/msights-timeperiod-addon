`import Ember from 'ember'`

IndexController= Ember.Controller.extend
  config:
    options: [
      {type:'current-week', label:'Current Week', options: {fulldate_range_checkbox: true}},
      {type:'current-month', label:'Current Month', options: {fulldate_range_checkbox: true}},
      {type:'current-quarter', label:'Current Quarter', options: {fulldate_range_checkbox: true}},
      {type:'current-year', label:'Current Year', options: {fulldate_range_checkbox: true}},
      {type:'current-fiscal-quarter', label:'Current Fiscal Quarter', options: {fulldate_range_checkbox: true}},
      {type:'current-fiscal-year', label:'Current Fiscal Year', options: {fulldate_range_checkbox: true}},
      {type:'current-broadcast-month', label:'Current Broadcast Month', options: {fulldate_range_checkbox: true}},
      {type:'current-broadcast-quarter', label:'Current Broadcast Quarter', options: {fulldate_range_checkbox: true}},
      {type:'last-week', label:'Last Week'},
      {type:'last-month', label:'Last Month'},
      {type:'last-quarter', label:'Last Quarter'},
      {type:'last-year', label:'Last Year'},
      {type:'last-fiscal-quarter', label:'Last Fiscal Quarter'},
      {type:'last-fiscal-year', label:'Last Fiscal Year'},
      {type:'last-broadcast-month', label:'Last Broadcast Month'},
      {type:'last-broadcast-quarter', label:'Last Broadcast Quarter'},
      {type:'last-number-of-period', label:'Last [number] of [period]', options: {number_field: true, period_selector: true}},
      {type:'custom-period', label:'Custom Period', options: {custom_period_fields:true}}
    ],
    searchBy: 'label',
    periods: [
      'Days','Weeks','Months','Quarters','Years','Fiscal Quarters',
      'Fiscal Years','Broadcast Months','Broadcast Quarters','Broadcast Years'
    ],
    datepicker: {startDate: '01/01/2012', endDate: new Date}
    title: {currentDate: new Date, throughDate: '01/01/2012', integrationDate: new Date}

`export default IndexController`
