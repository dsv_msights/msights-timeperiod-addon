`import Ember from 'ember'`

TitleController = Ember.Controller.extend
  config:
    title: '3.- Time Period',
    currentDate: '01/01/2016',
    throughDate: '01/01/2016',
    integrationDate: '01/01/2016'

`export default TitleController`
