import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('examples', function() {
    this.route('timeperiod', function(){
      this.route('title');
      this.route('checkbox');
      this.route('label');
      this.route('datepicker');
      this.route('selector');
    });
  });
});

export default Router;
