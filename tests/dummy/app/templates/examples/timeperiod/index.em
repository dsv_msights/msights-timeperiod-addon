div.panel.panel-default
  div.panel-body
    = msights-timeperiod-title [
      title='1.- Time Period Container'
      currentDate=config.title.currentDate
      throughDate=config.title.throughDate
      integrationDate=config.title.integrationDate
    ]
    div.col-xs-12
      = msights-timeperiod-selector [
        options=config.options
        searchBy=config.searchBy
        periods=config.periods
        startDate=config.datepicker.startDate
        endDate=config.datepicker.endDate
      ]

= config.title.fromDate
