h3 msights-timeperiod-selector
= msights-timeperiod-selector [
  options=config.options
  searchBy=config.searchBy
  periods=config.periods
  startDate=config.datepicker.startDate
  endDate=config.datepicker.endDate
]
