h3 Title Component
= msights-timeperiod-title [
  currentDate=config.currentDate
  throughDate=config.throughDate
  integrationDate=config.integrationDate
  title=config.title
]
