`import { test, moduleForComponent } from 'ember-qunit'`
`import hbs from 'htmlbars-inline-precompile'`

moduleForComponent 'msights-timeperiod-checkbox', 'Integration | Component | msights timeperiod checkbox', {
  integration: true
}

test 'it renders', (assert) ->
  assert.expect 2

  # Set any properties with @set 'myProperty', 'value'
  # Handle any actions with @on 'myAction', (val) ->

  @render hbs """{{msights-timeperiod-checkbox}}"""

  assert.equal @$().text().trim(), ''

  # Template block usage:
  @render hbs """
    {{#msights-timeperiod-checkbox}}
      template block text
    {{/msights-timeperiod-checkbox}}
  """

  assert.equal @$().text().trim(), 'template block text'
