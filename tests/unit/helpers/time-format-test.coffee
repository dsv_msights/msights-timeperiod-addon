`import { timeFormat } from 'dummy/helpers/time-format'`
`import { module, test } from 'qunit'`

module 'Unit | Helper | time format'

# Replace this with your real tests.
test 'get a date format from a Date class', (assert) ->
  result = timeFormat [new Date '01/01/2016']
  assert.equal(result, '1-Jan-2016')
