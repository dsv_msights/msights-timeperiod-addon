`import Ember from 'ember'`
`import layout from '../templates/components/msights-timeperiod-selector'`
`import TimePeriodError from '../errors/timeperiod-error'`

###*
# @namespace {defaultOptions} Object that contains a set of pre-defined time periods
# @property {String} type Selector option value
# @property {String} label Selector option text
# @property {Object} options
#  Set of flags that displays extra fields on template:
#  - fulldate_range_checkbox: Checkbox component used by 'Current' options that indicates a full date range method
#  - number_field:            Input component used by a '[number]' option that captures a number value
#  - period_selector:         Selector component used by a '[period]' option that selects a valid period value
#  - custom_period_fields:    Datepicker components used by a 'custom' option that captures From/To date times.
###
defaultOptions = [
  {type:'current-week', label:'Current Week', options: {fulldate_range_checkbox: true}},
  {type:'current-month', label:'Current Month', options: {fulldate_range_checkbox: true}},
  {type:'current-quarter', label:'Current Quarter', options: {fulldate_range_checkbox: true}},
  {type:'current-year', label:'Current Year', options: {fulldate_range_checkbox: true}},
  {type:'current-fiscal-quarter', label:'Current Fiscal Quarter', options: {fulldate_range_checkbox: true}},
  {type:'current-fiscal-year', label:'Current Fiscal Year', options: {fulldate_range_checkbox: true}},
  {type:'current-broadcast-month', label:'Current Broadcast Month', options: {fulldate_range_checkbox: true}},
  {type:'current-broadcast-quarter', label:'Current Broadcast Quarter', options: {fulldate_range_checkbox: true}},
  {type:'last-week', label:'Last Week'},
  {type:'last-month', label:'Last Month'},
  {type:'last-quarter', label:'Last Quarter'},
  {type:'last-year', label:'Last Year'},
  {type:'last-fiscal-quarter', label:'Last Fiscal Quarter'},
  {type:'last-fiscal-year', label:'Last Fiscal Year'},
  {type:'last-broadcast-month', label:'Last Broadcast Month'},
  {type:'last-broadcast-quarter', label:'Last Broadcast Quarter'},
  {type:'last-number-of-period', label:'Last [number] of [period]', options: {number_field: true, period_selector: true}},
  {type:'custom-period', label:'Custom Period', options: {custom_period_fields:true}}
]
###*
# @namespace {periods} Array Set of pre-defined periods for the period_selector component
###
periods = [
  'Days','Weeks','Months','Quarters','Years','Fiscal Quarters',
  'Fiscal Years','Broadcast Months','Broadcast Quarters','Broadcast Years'
]

###*
# SelectorComponent Class
#
# @class SelectorComponent
# @extends Ember.Component
###
SelectorComponent = Ember.Component.extend
  ###*
  # Tag name that contains the layout
  #
  # @property tagName
  # @type {string}
  # @default "msights-timeperiod-selector"
  # @example
  #   <msights-timeperiod-selector>{{outlet}}</msights-timeperiod-selector>
  ###
  tagName: 'msights-timeperiod-selector',
  ###*
  # Layout Template
  #
  # @property layout
  # @type {Object}
  ###
  layout: layout,
  ###*
  # Class Constructor
  #
  # @method init
  ###
  init: ->
    @_super(arguments...)
    @setOptions('options')
    @selectOptionByDefault(0)
    @setSearchKey()
    @setPeriods()
    @setDatepicker('startDate', 'endDate')
  ###*
  # Set the selector options.
  #
  # @method setOptions
  # @param {String} prop Component property
  ###
  setOptions: (prop)->
    if @get(prop)
      @set('options', @get(prop)).map (item)->
        # TODO: Validate the options here...
        return item
    else
      @set('options', defaultOptions);
  ###*
  # Select an option by default
  #
  # @method selectOptionByDefault
  # @param {integer} index Array(options) index
  ###
  selectOptionByDefault: (index)->
    if @get('options').length >= index
      @set('selectedOption', @get('options')[index])
    else
      throw new TimePeriodError 'Cannot select the option[' + index +'] by default.'
  ###*
  # Set the key used to search for available options
  #
  # @method setSearchKey
  # @param {String} prop Component property
  ###
  setSearchKey: (prop='searchBy')->
    if @get(prop)
      @set('searchBy', @get(prop))
    else
      @set('searchBy', 'label')
  ###*
  # Set periods used by the period_selector component
  #
  # @method setPeriods
  # @param {String} prop Component property
  ###
  setPeriods: (prop='periods')->
    localPeriods = @get(prop)
    if localPeriods && localPeriods instanceof Array && localPeriods.length > 0
      @set('periods', localPeriods).map (item)->
        # TODO: Validate the periods here...
        return item
    else
      @set('periods', periods)
  ###*
  # Set Datepicker default options (startDate, endDate)
  #
  # @method setDatepicker
  # @param {String} startProp start date limit
  # @param {String} endProp finish date limit
  ###
  setDatepicker: (startProp, endProp)->
    if @get(startProp)
      @set('startDate', @get(startProp))
    if @get(endProp)
      @set('endDate', @get(endProp))
    else
      @set('endDate', new Date)
  actions:
    ###*
    # Action that changes the selected option.
    #
    # @method selectOption
    # @param {Object} option Object selected fro the available options
    ###
    selectOption: (option) ->
      @set('selectedOption', option)

`export default SelectorComponent`
