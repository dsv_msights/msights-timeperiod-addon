`import Ember from 'ember'`
`import layout from '../templates/components/msights-timeperiod-label'`
`import TimePeriodError from '../errors/timeperiod-error'`

###*
# Label Component
#
# @class LabelComponent
# @extends Ember.Component
# @constructor
###
LabelComponent = Ember.Component.extend
  ###*
  # Tag name that contains the layout
  #
  # @property tagName
  # @type {string}
  # @default "msights-timeperiod-label"
  # @example
  #   <msights-timeperiod-label>{{outlet}}</msights-timeperiod-label>
  ###
  tagName: 'msights-timeperiod-label',
  ###*
  # Layout Template
  #
  # @property layout
  # @type {Object}
  ###
  layout: layout

`export default LabelComponent`
