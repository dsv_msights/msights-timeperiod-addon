`import Ember from 'ember'`
`import layout from '../templates/components/msights-timeperiod-checkbox'`
`import TimePeriodError from '../errors/timeperiod-error'`

###*
# CheckBox Component
#
# @class CheckBoxComponent
# @extends Ember.Component
# @constructor
###
CheckBoxComponent = Ember.Component.extend
  ###*
  # Tag name that contains the layout
  #
  # @property tagName
  # @type {string}
  # @default "msights-timeperiod-checkbox"
  # @example
  #   <msights-timeperiod-checkbox>{{outlet}}</msights-timeperiod-checkbox>
  ###
  tagName: 'msights-timeperiod-checkbox',
  ###*
  # Layout Template
  #
  # @property layout
  # @type {Object}
  ###
  layout: layout

`export default CheckBoxComponent`
