`import Ember from 'ember'`
`import layout from '../templates/components/msights-timeperiod-datepicker'`
`import TimePeriodError from '../errors/timeperiod-error'`

###*
# Datepicker Component
#
# @class DatePickerComponent
# @extends Ember.Component
###
DatePickerComponent = Ember.Component.extend
  ###*
  # Tag name that contains the layout
  #
  # @property tagName
  # @type {string}
  # @default "msights-timeperiod-datepicker"
  # @example
  #   <msights-timeperiod-datepicker>{{outlet}}</msights-timeperiod-datepicker>
  ###
  tagName: 'msights-timeperiod-datepicker',
  ###*
  # Layout Template
  #
  # @property layout
  # @type {Object}
  ###
  layout: layout,
  ###*
  # Class Constructor
  #
  # @method init
  ###
  init: ->
    @_super(arguments...)
    currentDate = new Date
    if @get('value')
      @setDate('value')
    else
      @set('value', currentDate)
    if @get('startDate')
      @setDate('startDate')
    else
      @set('startDate', new Date '12/23/2012')
    if @get('endDate')
      @setDate('endDate')
    else
      @set('endDate', currentDate)
  ###*
  # Set a date property by type.
  #
  # @method setDate
  # @param {String} prop Property name used to retrive the value of a property from the component.
  # @return {Date} Returns a Date object on success
  # @throws {TimePeriodError} Invalid declaration
  ###
  setDate: (prop)->
    date = @get(prop)
    if typeof date == 'string'
      @set(prop, new Date date)
    else if date instanceof Date
      @set(prop, date)
    else
      throw new TimePeriodError 'Cannot declare the property ' + prop

`export default DatePickerComponent`
