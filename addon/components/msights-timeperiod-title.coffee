`import Ember from 'ember'`
`import layout from '../templates/components/msights-timeperiod-title'`
`import TimePeriodError from '../errors/timeperiod-error'`

###*
# Title Component
#
# @class TitleComponent
# @extends Ember.Component
###
TitleComponent = Ember.Component.extend
  ###*
  # Tag name that contains the layout
  #
  # @property tagName
  # @type {string}
  # @default "msights-timeperiod-title"
  # @example
  #   <msights-timeperiod-title>{{outlet}}</msights-timeperiod-title>
  ###
  tagName: 'msights-timeperiod-title',
  ###*
  # Layout Template
  #
  # @property layout
  # @type {Object}
  ###
  layout: layout,
  ###*
  # Class constructor
  #
  # @method init
  ###
  init: ->
    @_super(arguments...)
    self = @
    args = ['throughDate', 'integrationDate', 'currentDate']
    for value in args
      do (value) ->
        date = self.get(value)
        if date
          if typeof date == 'string'
            self.set(value, new Date date)
          else if date instanceof Date
            self.set(value, date)
          else
            throw new TimePeriodError 'Cannot declare the argument ' + value
        else
          self.set(value, new Date)

`export default TitleComponent`
