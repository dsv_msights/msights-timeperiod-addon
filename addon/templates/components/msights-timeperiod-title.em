div.col-xs-4
  = title
div.col-xs-8.text-right Available From: {{time-format throughDate}}
  Through: {{time-format currentDate}}
  (Last integration on {{time-format integrationDate}})
= yield
