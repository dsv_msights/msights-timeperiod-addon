div.row
  div.form-group.col-xs-4.power-selector-container
    = msights-timeperiod-label
      | Time Period
    = power-select [
      options=options
      selected=selectedOption
      searchField=searchBy
      onchange=(action "selectOption")
      class="form-control"
    ] as |option|
      = option.label
  if selectedOption.options.custom_period_fields
    div.form-group.col-xs-4
      = msights-timeperiod-label
        | Date From
      = msights-timeperiod-datepicker startDate=startDate endDate=endDate
    div.form-group.col-xs-4
      = msights-timeperiod-label
        | Date To
      = msights-timeperiod-datepicker startDate=startDate endDate=endDate
  if selectedOption.options.number_field
    div.form-group.col-xs-4
      = msights-timeperiod-label
        | [number]
      = input type='text' class='form-control'
  if selectedOption.options.period_selector
    div.form-group.col-xs-4
      = msights-timeperiod-label
        | [period]
      = power-select [
        options=periods
        selected=selectedPeriod
        onchange=(action (mut selectedPeriod))
      ] as |period|
        = period
      = msights-timeperiod-checkbox id='period-checkbox' checked=true
        | Include current
  if selectedOption.options.fulldate_range_checkbox
    div.col-xs-12
      = msights-timeperiod-checkbox
        | Include full date range (including future dates)
