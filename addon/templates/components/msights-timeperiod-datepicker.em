= bootstrap-datepicker [
  class="form-control"
  autoclose=true
  todayHighlight=true
  value=value
  startDate=startDate
  endDate=endDate
]
