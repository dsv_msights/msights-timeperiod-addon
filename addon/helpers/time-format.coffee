`import Ember from 'ember'`

###*
# @namespace {monthNames} Array Set of avaiable months in string format.
###
monthNames = [
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

###*
# My method description.  Like other pieces of your comment blocks,
# this can span multiple lines.
#
# @param {Date} date Javascript Datetime instance
# @return {String} Returns the time format (dd-<monthName>-YY)
###
timeFormat = ([date]) ->
  return date.getDate() + "-" + monthNames[date.getMonth()] + "-" + date.getFullYear()

TimeFormatHelper = Ember.Helper.helper timeFormat

`export { timeFormat }`
`export default TimeFormatHelper`
