`import Ember from 'ember'`

TimePeriodError = (message) ->
  @message = message
  last_part = new Error().stack.match(/[^\s]+$/);
  @stack = @name + ' at ' + last_part

TimePeriodError.prototype = Object.create(Error.prototype)
TimePeriodError.prototype.name = 'TimePeriodError';
TimePeriodError.prototype.message = "";
TimePeriodError.prototype.constructor = TimePeriodError;

`export default TimePeriodError`
