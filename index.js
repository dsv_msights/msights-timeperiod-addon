/* jshint node: true */
'use strict';

module.exports = {
  name: 'msights-timeperiod-addon',
  isDevelopingAddon: function() {
    return true;
  },
  included: function(app){
    this._super.included(app);

    app.import(app.bowerDirectory + '/bootstrap-sass/assets/javascripts/bootstrap.js');
  }
};
