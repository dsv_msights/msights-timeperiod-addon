/*jshint node:true*/
'use strict';

module.exports = function(environment, appConfig) {
  var ENV = {
    // Here you can pass flags/options to your consuming application
  }

  if (environment === 'development') {
    ENV.coffeeOptions = {
      blueprints: true
    };
    ENV.emblemOptions = {
      blueprints: false,
      debugging: true
    };
  }
  if (environment === 'production') {}

  return ENV;
};
